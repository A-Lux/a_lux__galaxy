var owl = $('.owl-carousel');
$('.owl-carousel').owlCarousel({
    items: 3, 
    loop:true,
    dots:false,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
        	items:3
        }
    }
})


// Go to the next item
$('.arrow-right').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.arrow-left').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
})