// Create a function to easily select our elements
const toBeSlected = (s) => document.querySelector(s);
var x = 0;
// Hide and show the navigation on click
toBeSlected('.menu-toggle').addEventListener('click', () =>{
	x++;
	if(x%2==1){
		document.getElementsByTagName("body")[0].style.overflow = "hidden";
	} else{
		document.getElementsByTagName("body")[0].style.overflow = "scroll";
	}
	toBeSlected('section').classList.toggle('nav-open');

});
